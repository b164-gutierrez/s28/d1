// 2.
console.log(fetch('https://jsonplaceholder.typicode.com/todos'));


// 3.
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET'
})
.then(response => response.json())
.then(data => {console.log(data)
})


// 4.



// 5.

fetch('https://jsonplaceholder.typicode.com/todos?id=1')
.then(res => res.json())
.then(data => console.log(data));

// 6.


// 7.
fetch('https://jsonplaceholder.typicode.com/todos', {

	method: 'POST',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'New todos',
		completed: true
		
	})

})
.then(response => response.json())
.then(json => console.log(json));


// 8.
fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		id: 1,
		title: 'Updated post',
		completed: true,
		
	})
})
.then(res => res.json())
.then(data => console.log(data));

// 9.

fetch('https://jsonplaceholder.typicode.com/todos/4', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'new Updated list',
		description: '',
		status: 'Updated',
		dateCompleted: "",
		userId: 4
		
	})
})
.then(res => res.json())
.then(data => console.log(data));



// 10.

fetch('https://jsonplaceholder.typicode.com/todos/5', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		
		title: 'Updated post'
		})
})
.then(res => res.json())
.then(data => console.log(data));

// 11.

fetch('https://jsonplaceholder.typicode.com/todos/6', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		
		completed: true,
		dateCompleted: Date
		})
})
.then(res => res.json())
.then(data => console.log(data));

// 12.

fetch('https://jsonplaceholder.typicode.com/todos/7', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data));

// 13.