			// POSTMAN 03-29-22

// Javascript Synchronous vs Asynchronous

				// 1. Synchronous Javascript
// javascript is by default is synchronous - meaning that only one statement run/execute at a time.

// sample
// console.log('Hello World');

// console.log('Goodbye');

// for(let i = 0; i <= 100; i++){
// 	console.log(i);
// }

// blocking - a slow process of code.
// console.log('Hello World');



				// 2. Asynchronous Javascript

 // sample
 // function printMe(){
 // 	console.log('print me');
 // }
 // function test(){
 // 	console.log('print me');
 // }
 // setTimeout(printMe, 5000);
 // test();



// fetch() is a method in JS, which allows us to send a request to an API and process its response.

// the fetch API allows you to asynchronous request for a resource (data).
// the fetch recieves a PROMISE.
// a 'Promise' is an object that represents the evetual completion (or failure) of an asynchronous
 
// url - the url to resource/routes from the Server.

// optional objects - contains additional information about request such as method, the body and headers.

// it parse the response as JSON

/*
	syntax:
			fetch(url, {options}).then(response => response.json()).then(data => {console.log(data)})
*/

/*a promise may be in one of 3 possible states: 
1. fulfilled 
2. rejected 
3. pending*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// retrieves all post (GET)

fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'GET'
})
.then(response => response.json())
.then(data => {console.log(data)
})

// .then method captures the response object and returns another 'promise' which will eventually be 'resolved' or 'rejected'.


// 'async' and 'await' keywords is another approach that can be used to achieve asynchronous code.

// used in functions to indicate which portions of code should be awaited for.

async function fetchData(){

	let result =await fetch('https://jsonplaceholder.typicode.com/posts');

	// result returned by fetch
	console.log(result);

	console.log(typeof result);

	console.log(result.body);

	// convert data from response object as JSON
	let json = await result.json();

	console.log(json);
}
fetchData();


// Creating a post
// create new post following the REST API (create, POST)
fetch('https://jsonplaceholder.typicode.com/posts', {

	method: 'POST',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World',
		userId: 1
	})

})
.then(response => response.json())
.then(json => console.log(json));



// Updating a post
// update a specific post following the REST API (update, PUT)
// is a method modifying resources where the client sends data that updates the ENTIRE resources. It is used to set an entity's information completely.

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body: 'Hello Again',
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data));



// Patch method
// is a method applies a partial update to the resources.
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		
		title: 'Updated post'
		})
})
.then(res => res.json())
.then(data => console.log(data));


// Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data));



// Filtering posts
// data can be filtered by sending the userId along with the URL.
// ?
// individual parameters

/*
	syntax:
			'url?parameterName=value'
*/
	
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(res => res.json())
.then(data => console.log(data));

// multiple parameters
/*
	syntax:
			'url?parameterNameA=valueA&paramB=valueB'
*/
	



// retrieving nested/related data
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(res => res.json())
.then(data => console.log(data));

// mini activity 03-29-22
// https://jsonplaceholder.typicode.com/todos